/*
 * * Copyright 2017-2018 github.com/BrokenEarthDev
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.brokenearthdev.simpleyaml.api;

import me.brokenearthdev.simpleyaml.events.YamlCloneEvent;
import me.brokenearthdev.simpleyaml.utils.CachedFileData;
import me.brokenearthdev.simpleyaml.utils.FileWriter;

import java.io.*;
import java.net.URL;

/**
 * This class is responsible for cloning yaml files, {@link InputStream}, or
 * {@link URL}
 *
 * @author BrokenEarth // BrokenEarthDev
 * @version 1.0
 * @since 1.0
 */
public final class YamlCloner {

    /**
     * The {@link InputStream} initialized by {@link #setCloneData(InputStream, File)}
     */
    private InputStream instream;

    /**
     * The output file initialized by the setCloneData methods
     */
    private File outFile;

    /**
     * Sets the clone data with the {@link InputStream} and the output {@link File}
     * specified. Set the clone data before calling {@link #cloneYaml()}
     *
     * @param stream The {@link InputStream} in where the yaml data is in
     * @param out The output {@link File} after cloning
     */
    public void setCloneData(InputStream stream, File out) {
        this.outFile = out;
        this.instream = stream;
    }

    /**
     * Sets the clone data with the {@link URL} and the output {@link File}
     * specified. Set the clone data before calling {@link #cloneYaml()}
     *
     * @param url The {@link URL} in where tje yaml data is found. If there
     *            are any issues with communicating or resolving host,
     *            {@link IOException} will be thrown
     * @param out The output {@link File} after cloning
     * @throws IOException If there are issues with opening a {@link URL} stream
     * by using {@link URL#openStream()}
     */
    public void setCloneData(URL url, File out) throws IOException {
        setCloneData(url.openStream(), out);
    }

    /**
     * Sets the clone data with the input {@link File} and the output
     * {@link File} specified. Set the clone data before calling
     * {@link #cloneYaml()}
     *
     * @param origin The input file that will be cloned to the output
     *               file
     * @param out The output {@link File} after cloning
     * @throws IOException If there are issues with getting an {@link InputStream}
     * of a file
     */
    public void setCloneData(File origin, File out) throws IOException {
        FileInputStream instream = new FileInputStream(origin);
        setCloneData(instream, out);
    }

    /**
     * Clones the yaml data with the data set and returns the instance
     * of the cloned file. Output files will have their contents replaced
     * with the specified input data contents set
     *
     * @return The cloned yaml
     * @throws IOException Thrown when there is an issue with the file
     * or can't write to the output file
     * @throws NullPointerException If the clone data isn't set. To set
     * the clone data, use {@link #setCloneData(InputStream, File)},
     * {@link #setCloneData(URL, File)}}, or {@link #setCloneData(File, File)}
     */
    public Yaml cloneYaml() throws IOException {
        if (instream == null || outFile == null) throw new NullPointerException("Clone data isn't set");
        String text = new CachedFileData(instream).getContents();
        FileWriter writer = new FileWriter(outFile);
        writer.write(text, true);
        Yaml yaml = new Yaml(text);
        Yaml.EVENT_BUS.callEvent(new YamlCloneEvent(outFile, instream, yaml));
        return yaml;
    }

}
