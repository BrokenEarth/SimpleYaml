/*
 * Copyright 2018 github.com/BrokenEarthDev
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */
package me.brokenearthdev.simpleyaml.api;


import java.util.TimeZone;

/**
 * This class is responsible for changing the YAML options and
 * storing the options.
 * Every {@link Yaml} has a different object of this class.
 *
 * @author ReflxctionDev // BrokenEarthDev
 * @version 1.0
 * @since 1.0
 */
public final class YamlOptions {

    /**
     * This enum has has 3 constants. {@link #WIN} represents the
     * Windows LineBreak, {@link #MAC} represents the Mac LineBreak and
     * {@link #UNIX} represents the Unix LineBreak
     */
    public enum LineBreak {

        /**
         * Represents the Windows LineBreak
         */
        WIN("\r\n"),

        /**
         * Represents the Mac LineBreak
         */
        MAC("\r"),

        /**
         * Represents the Unix LineBreak
         */
        UNIX("\n");

        /**
         * The linebreak character initialized by {@link #LineBreak(String)}
         */
        private String lineBreak;

        /**
         * A constructor that initializes the LineBreak for each constant
         *
         * @param lineBreak The LineBreak
         */
        LineBreak(String lineBreak) {
            this.lineBreak = lineBreak;
        }

        /**
         * @return The linebreak style
         */
        public String getLineBreak() {
            return lineBreak;
        }

        @Override
        public String toString() {
            return lineBreak;
        }
    }

    /**
     * The enum has 4 constants representing YAML scalar style.
     * {@link #DOUBLE_QUOTED} represents two quote marks, {@link #SINGLE_QUOTED}
     * represents 1 quote mark, {@link #LITERAL} represents a pipe symbol,
     * {@link #FOLDED} represents a greater than sign, and {@link #PLAIN} is the
     * default option
     */
    public enum ScalarStyle {

        /**
         * Represents two quote marks
         */
        DOUBLE_QUOTED('"'),

        /**
         * Represents one quote mark
         */
        SINGLE_QUOTED('\''),

        /**
         * Represents a pipe symbol
         */
        LITERAL('|'),

        /**
         * Represents a greater than sign
         */
        FOLDED('>'),

        /**
         * Represents the default option
         */
        PLAIN('\0');

        /**
         * The scalar style character initialized by {@link #setScalarStyle(ScalarStyle)}
         */
        private Character styleChar;

        ScalarStyle(Character style) {
            this.styleChar = style;
        }

        /**
         * @return The scalar style character
         */
        public char getChar() {
            return styleChar;
        }

        @Override
        public String toString() {
            return styleChar + "";
        }
    }

    /**
     * Block styles use indentation to denote nesting and scope within the
     * document. In contrast, flow styles rely on explicit indicators to denote
     * nesting and scope.
     *
     * @see <a href="http://www.YAML.org/spec/current.html#id2509255">3.2.3.1.
     * Node Styles (http://YAML.org/spec/1.1)</a>
     */
    public enum FlowStyle {

        FLOW(Boolean.TRUE),

        BLOCK(Boolean.FALSE),

        AUTO(null);

        private Boolean styleBoolean;

        FlowStyle(Boolean flowStyle) {
            styleBoolean = flowStyle;
        }

        public Boolean getStyleBoolean() {
            return styleBoolean;
        }

        @Override
        public String toString() {
            return styleBoolean + "";
        }
    }

    /**
     * The indentation amount. You can set the indentation amount by calling
     * method {@link #setIndent(int)}
     */
    private int indent = 2;

    /**
     * The {@link LineBreak} set. You can set the {@link LineBreak} by calling
     * method {@link #setLineBreak(LineBreak)}
     */
    private LineBreak lineBreak = LineBreak.UNIX;

    /**
     * The {@link TimeZone} set. You can set the {@link TimeZone} by calling
     * method {@link #setTimeZone(TimeZone)}
     */
    private TimeZone timeZone = null;

    /**
     * The header set. You can set the header by calling method
     * {@link #setHeader(String)}
     */
    private String header = null;

    /**
     * The footer set. You can set the footer by calling method {@link #setFooter(String)}
     */
    private String footer = null;

    /**
     * The {@link ScalarStyle} set. You can set the {@link ScalarStyle} by calling method
     * {@link #setScalarStyle(ScalarStyle)}
     */
    private ScalarStyle scalarStyle = ScalarStyle.PLAIN;

    /**
     * The {@link FlowStyle} set. You can set the {@link FlowStyle} by calling method
     * {@link #setFlowStyle(FlowStyle)}
     */
    private FlowStyle flowStyle = FlowStyle.BLOCK;

    /**
     * Specifies whether if the unicode is allowed or not. You can set whether unicode
     * is allowed or not by calling method {@link #setScalarStyle(ScalarStyle)}
     */
    private boolean allowUnicode = true;

    /**
     * Specifies whether if the YAML is locked or not. Locking a {@link Yaml} will
     * not allow read/save changes
     */
    private boolean locked = false;

    /**
     * The path separator set. You can set the path separator by calling method
     * {@link #setSeparator(char)}.
     * A separator indicates to look for the nested path under the parent path
     * <br> For example: <br>
     * A YAML file:
     * <pre>
     *     one:
     *       two: 3
     * </pre>
     * <br>To get "two" nested within "one", use YAML.get(one(separator)two)
     */
    private char separator = '.';

    /**
     * Indentation allows YAML to be in a better format, easily readable. This will
     * affect especially nested paths or value
     * @return The indentation of the yaml file
     */
    public int getIndent() {
        return indent;
    }

    /**
     * @return The {@link LineBreak} set. If not set, this method will return
     * {@link LineBreak#UNIX}
     */
    public LineBreak getLineBreak() {
        return lineBreak;
    }

    /**
     * @return The {@link TimeZone} set. If not set, this method will return null
     */
    public TimeZone getTimeZone() {
        return timeZone;
    }

    /**
     * @return The header set. Headers are found on the top of a YAML file. If not set,
     * this method will return null
     */
    public String getHeader() {
        return header;
    }

    /**
     * @return The footer set. Footers are found on the bottom of a YAML file. If not set,
     * this method will return null
     */
    public String getFooter() {
        return footer;
    }

    /**
     * The scalar style used in the writer
     *
     * @return The scalar style used in the writer
     */
    public ScalarStyle getScalarStyle() {
        return scalarStyle;
    }

    /**
     * The flow style used by the writer
     *
     * @return The flow style used by the writer
     */
    public FlowStyle getFlowStyle() {
        return flowStyle;
    }

    /**
     * Whether the writer is allowed to use unicode
     *
     * @return Whether the writer is allowed to use unicode
     */
    public boolean isAllowUnicode() {
        return allowUnicode;
    }

    /**
     * Whether the YAML writer is locked or not. If {@code true}, any attempt
     * to write to the file will throw a {@link IllegalArgumentException}
     *
     * @return Whether the writer is locked or not
     */
    public boolean isLocked() {
        return locked;
    }

    /**
     * The separator character used on line breaks. This is a dot (.) by default
     *
     * @return The separator character
     */
    public char getSeparator() {
        return separator;
    }

    /**
     * Sets the indent before every line.
     *
     * @param indent New value to set
     */
    public void setIndent(int indent) {
        this.indent = indent;
    }

    /**
     * Sets the line break character.
     *
     * @param lineBreak New value to set
     */
    public void setLineBreak(LineBreak lineBreak) {
        this.lineBreak = lineBreak;
    }

    /**
     * Sets the timezone, if you dump dates, the dates output will be in the timezone equal to
     * the timezone set. If timezone isn't set, the output would be the system time
     *
     * @param zone New value to set
     */
    public void setTimeZone(TimeZone zone) {
        this.timeZone = zone;
    }

    /**
     * Sets the header of the YAML file, which is a set of comments in the beginning of the file
     *
     * @param header Header to use
     */
    public void setHeader(String header) {
        this.header = header;
    }

    /**
     * Sets the footer of the YAML file, which is a set of comments at the end of the file
     *
     * @param footer Footer to use
     */
    public void setFooter(String footer) {
        this.footer = footer;
    }

    /**
     * Sets the scalar style used by the writer
     *
     * @param scalarStyle New value to set
     */
    public void setScalarStyle(ScalarStyle scalarStyle) {
        this.scalarStyle = scalarStyle;
    }

    /**
     * Sets the flow style used by the writer
     *
     * @param style New value to set
     */
    public void setFlowStyle(FlowStyle style) {
        this.flowStyle = style;
    }

    /**
     * Sets whether using unicode in the writer is allowed or not
     *
     * @param allowUnicode New value to set
     */
    public void setAllowUnicode(boolean allowUnicode) {
        this.allowUnicode = allowUnicode;
    }

    /**
     * Sets whether the writer is locked or not.
     *
     * @param locked New value to set
     * @see #isLocked()
     */
    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    /**
     * Sets the separator character which breaks a new line and adds indentation
     *
     * @param separator New value to set
     */
    public void setSeparator(char separator) {
        this.separator = separator;
    }

}
