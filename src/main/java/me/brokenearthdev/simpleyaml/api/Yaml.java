/*
 * * Copyright 2017-2018 github.com/BrokenEarthDev
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.brokenearthdev.simpleyaml.api;

import me.brokenearthdev.eventbus.entities.EventBus;
import me.brokenearthdev.simpleyaml.entities.YamlReaderWriter;
import me.brokenearthdev.simpleyaml.entities.YamlReader;
import me.brokenearthdev.simpleyaml.entities.YamlWriter;
import me.brokenearthdev.simpleyaml.events.Event;
import me.brokenearthdev.simpleyaml.exceptions.YamlException;
import me.brokenearthdev.simpleyaml.parser.YamlParserImpl;
import me.brokenearthdev.simpleyaml.utils.CachedData;
import me.brokenearthdev.simpleyaml.utils.CachedFileData;
import me.brokenearthdev.simpleyaml.entities.YamlParser;
import me.brokenearthdev.simpleyaml.utils.StreamUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * This class supports reading and writing to a yaml file because
 * it inherits from an implementation class which is {@link YamlReaderWriter}
 * which inherits from {@link YamlReader} and {@link YamlWriter} which inherits from
 * {@link YamlReader} and {@link YamlWriter}
 * <p>
 * You can clone a yaml to a file using {@link YamlCloner}. Creating an object
 * of this class with the default constructor will initialize it as an empty string.
 * Creating an object of this class accepts a {@link String}, a {@link File}, an
 * {@link InputStream}, and {@link URL}
 * <p>
 * Every yaml has an option, which defines the indentation, yaml stile, and more.
 * {@link YamlOptions} object are created when you create an object of this class.
 * To get the option's instance, use {@link #getOptions()}
 *
 * @author BrokenEarth // BrokenEarthDev
 * @version 1.0
 * @see YamlOptions
 * @since 1.0
 */
public final class Yaml extends YamlReaderWriter {
    /**
     * The {@link YamlOptions} object which is created when
     * you create an object of this class
     */
    private YamlOptions options;

    private static final YamlOptions defaultOptions = new YamlOptions();

    /**
     * This constructor accepts a string as a parameter.
     * The default constructor will call this constructor as an empty
     * string as a parameter
     *
     * @param string The yaml file data, which can be retrieved
     *               using the reader methods
     */
    public Yaml(String string) {
        super(new YamlParserImpl(string));
        this.options = this.copy(defaultOptions);
        super.reloadOptions();
    }

    /**
     * This constructor accepts an {@link InputStream} as a parameter and gets
     * the data from the {@link InputStream} using {@link CachedFileData#getContents()}
     * and then calls the constructor which accepts a string as a parameter.
     * {@link #Yaml(String)}
     *
     * @param stream The yaml file data input stream which will be converted
     *               to a string and called to the main constructor with the converted
     *               string. The data can be retrieved using the reader methods
     */
    public Yaml(InputStream stream) {
        this(new CachedFileData(stream).getContents());
    }

    /**
     * This constructor accepts a {@link URL} as a parameter and
     * and then calls the constructor that accepts a {@link YamlParser}
     * and {@link URL} as their parameter.
     * The constructor will retrieve the {@link InputStream} from the {@link URL}
     * and then creates a {@link YamlParser} object using the retrieved {@link InputStream}
     *
     * @param url The url in which will constructor that accepts an {@link InputStream}
     *            as a parameter ({@link #Yaml(InputStream)})
     * @throws IOException This exception is thrown when there is an issue with
     *                     reading a connection or resolving a host from the provided url. If the
     *                     provided url is invalid, the exception will also be thrown
     */
    public Yaml(URL url) throws IOException {
        super(new YamlParserImpl(url.openStream()), url);
        this.options = this.copy(defaultOptions);
        super.reloadOptions();
    }

    /**
     * The constructor accepts a {@link File} as a parameter and gets
     * the {@link InputStream} from the file using {@link FileInputStream} and then
     * calls the constructor that requires a {@link YamlParser} and a {@link File}
     * as their parameters
     *
     * @param file The file in which will have the data retrieved and will be
     *             called to the constructor that accepts an {@link YamlParser}
     *             and a {@link File} as their parameters
     */
    public Yaml(File file) {
        super(new YamlParserImpl(StreamUtils.newStream(file)), file);
        this.options = copy(defaultOptions);
        reloadOptions();
    }

    /**
     * The default constructor will call the main constructor with an
     * empty string as a parameter ({@link #getString(String)}) will return null
     * if you didn't set any data because no file
     * data is specified. You can write the data to a string and save it
     * using {@link #saveToString(boolean)}
     */
    public Yaml() {
        this("");
    }

    /**
     * @return The default yaml options
     */
    public static YamlOptions getDefaultOptions() {
        return defaultOptions;
    }

    /**
     * The {@link EventBus} is responsible for registering and handling events.
     * To register an event listener, use {@link EventBus#register(Object)} and to
     * unregister an event listener, use {@link EventBus#unregister(Object)}.
     * <p>
     * To make an event handler method in your registered event listener class, annotate
     * the method with {@link me.brokenearthdev.eventbus.annotations.SubscribeEvent} and the method must have a parameter that
     * needs an {@link Event} object or any class inherited from the {@link Event} class.
     * After registering the class that contains the method, event methods would run if
     * an event is called.
     */
    public static final EventBus<Event> EVENT_BUS = new EventBus<>();

    /**
     * The {@link YamlOptions} is a class for setting yaml options. You can set
     * yaml options such as indentation and more. You can also set scalar styles
     * and flow styles. You can lock or unlock this yaml by using
     * {@link YamlOptions#setLocked(boolean)}. Locking this yaml won't allow you to set,
     * remove, and save data.
     *
     * @return A {@link YamlOptions} object, which is a class for setting yaml options
     */
    public YamlOptions getOptions() {
        return options;
    }

    /**
     * This method prettifies (improve its syntax) the yaml data as a string.
     * You can set the {@link YamlOptions} to set the preferred style such as
     * indentation, scalar style, flow style, and more.
     * However, if {@link YamlOptions#isLocked()} returns true, {@link YamlException}
     * will be thrown.
     *
     * @param string  The yaml data as a string
     * @param options This method also requires a {@link YamlOptions} object.
     *                You can set the value to null if you want to use the
     *                default options
     * @return The prettified yaml as a string
     */
    public static String prettifyYaml(String string, YamlOptions options) {
        Yaml yaml = new Yaml(string);
        if (options != null) yaml.options = options;
        return yaml.saveToString();
    }

    /**
     * This method prettifies (improve its syntax) the yaml data from a specified
     * {@link InputStream}. The method will retrieve the data as a string from the
     * {@link InputStream} using {@link CachedFileData#getContents()} and will call
     * the method that accepts a string as a parameter and {@link YamlOptions} as a
     * second parameter ({@link #prettifyYaml(String, YamlOptions)})
     * <p>
     * You can set the {@link YamlOptions} to set the preferred style such as
     * indentation, scalar style, flow style, and more.
     * However, if {@link YamlOptions#isLocked()} returns true, {@link YamlException}
     * will be thrown.
     *
     * @param instream A yaml's data input stream which will be converted
     *                 to a string
     * @param options  This method also requires a {@link YamlOptions} object.
     *                 You can set the value to null if you want to use the
     *                 default options
     * @return The prettified yaml as string
     */
    public static String prettifyYaml(InputStream instream, YamlOptions options) {
        CachedData data = new CachedFileData(instream);
        return prettifyYaml(data.getContents(), options);
    }

    /**
     * The method prettifies (improve it's syntax) the yaml data from a specified
     * {@link URL}. The method will retrieve the {@link InputStream} from the url
     * using {@link URL#openStream()} and calls the method that accepts an
     * {@link InputStream} and a {@link YamlOptions} as the parameter
     * ({@link #prettifyYaml(InputStream, YamlOptions)}) which will later retrieve
     * the data as a string and calls the method that accepts a {@link String} and a
     * {@link YamlOptions} as a parameter ({@link #prettifyYaml(String, YamlOptions)})
     * <p>
     * You can set the {@link YamlOptions} to set the preferred style such as
     * indentation, scalar style, flow style, and more.
     * However, if {@link YamlOptions#isLocked()} returns true, {@link YamlException}
     * will be thrown.
     *
     * @param url     The yaml's url which will be converted to an {@link InputStream}
     *                which will be converted to a String
     * @param options The method requires a {@link YamlOptions} object.
     *                You can set the value to null if you want to use
     *                the default options
     * @return The prettified yaml as a string
     * @throws IOException This exception is thrown when there is an issue with
     *                     reading a connection or resolving a host from the provided url. If the
     *                     provided url is invalid, the exception will also be thrown
     */
    public static String prettifyYaml(URL url, YamlOptions options) throws IOException {
        return prettifyYaml(url.openStream(), options);
    }

    /**
     * This method prettifies (improve its syntax) the yaml data as a string.
     * The method will call the method that accepts a string and a
     * {@link YamlOptions} as their parameters ({@link #prettifyYaml(String, YamlOptions)})
     * The {@link YamlOptions} will be set to the default {@link YamlOptions}
     *
     * @param string The yaml data as a string
     * @return The prettified yaml as a string
     */
    public static String prettifyYaml(String string) {
        return prettifyYaml(string, null);
    }

    /**
     * This method prettifies (improve its syntax) the yaml data from a specified
     * {@link InputStream}. The method will call the method that accepts an
     * {@link InputStream} and a {@link YamlOptions} as their parameters. Which
     * will retrieve the data from the specified {@link InputStream} and calls
     * the method that accepts a string and a {@link YamlOptions} as the parameters
     * ({@link #prettifyYaml(String, YamlOptions)})
     * The {@link YamlOptions} will be set to the default {@link YamlOptions}
     *
     * @param instream A yaml's data input stream which will be converted
     *                 to a string
     * @return The prettified yaml as string
     */
    public static String prettifyYaml(InputStream instream) {
        return prettifyYaml(instream, null);
    }

    /**
     * The method prettifies (improve it's syntax) the yaml data from a specified
     * {@link URL}. The method will call the method that accepts {@link URL} and
     * {@link YamlOptions} as their parameters ({@link #prettifyYaml(URL, YamlOptions)})
     * which will retrieve the {@link InputStream} from the url and calls the method that
     * accepts an {@link InputStream} and a {@link YamlOptions} as their parameters. The
     * called method will retrieve the data as a string from the {@link InputStream} and
     * will call the method that accepts a string and a {@link YamlOptions} as their parameters
     * ({@link #prettifyYaml(String, YamlOptions)})
     * The {@link YamlOptions} will be set to the default {@link YamlOptions}
     *
     * @param url The yaml's url which will be converted to an {@link InputStream}
     *            which will be converted to a String
     * @return The prettified yaml as a string
     * @throws IOException This exception is thrown when there is an issue with
     *                     reading a connection or resolving a host from the provided url. If the
     *                     provided url is invalid, the exception will also be thrown
     */
    public static String prettifyYaml(URL url) throws IOException {
        return prettifyYaml(url, null);
    }

    /**
     * Simply copies the options from the {@link YamlOptions} specified
     * in the parameter and returns a new instance of {@link YamlOptions}
     * with the same options as the one specified in the parameter
     *
     * @param options The options to copy from
     * @return A new instance of {@link YamlOptions} with the same options
     * as the one specified in the parameter
     */
    private YamlOptions copy(YamlOptions options) {
        YamlOptions options1 = new YamlOptions();
        options1.setAllowUnicode(options.isAllowUnicode());
        options1.setFlowStyle(options.getFlowStyle());
        options1.setFooter(options.getFooter());
        options1.setHeader(options.getHeader());
        options1.setIndent(options.getIndent());
        options1.setLineBreak(options.getLineBreak());
        options1.setScalarStyle(options.getScalarStyle());
        options1.setSeparator(options.getSeparator());
        options1.setTimeZone(options.getTimeZone());
        options1.setLocked(options.isLocked());
        return options1;
    }

}
