/*
 * Copyright 2018 github.com/BrokenEarthDev
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */
package me.brokenearthdev.simpleyaml.serialization;

/**
 * Contains the {@link Serializer} and {@link Deserializer} singleton
 *
 * @author BrokenEarth // BrokenEarthDev
 * @version 1.0
 * @since 1.0
 */
public class Serialization {

    /**
     * The {@link Serializer} object
     */
    public static final Serializer SERIALIZER = new Serializer();

    /**
     * The {@link Deserializer} object
     */
    public static final Deserializer DESERIALIZER = new Deserializer();

    /**
     * @param class_ The class to check whether if it is serializable or not
     * @return Whether if the class is serializable or not
     */
    public static boolean isSerializable(Class<?> class_) {
        for (Class<?> interface_ : class_.getInterfaces()) {
            if (interface_.equals(Serializable.class)) return true;
        }
        return false;
    }

    /**
     * @param o The object to check whether if it is serializable or not
     * @return Whether if the object is serializable or not
     */
    public static boolean isSerializable(Object o) {
        return isSerializable(o.getClass());
    }

}
