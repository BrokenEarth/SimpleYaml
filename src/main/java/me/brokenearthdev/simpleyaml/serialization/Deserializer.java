/*
 * Copyright 2018 github.com/BrokenEarthDev
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */
package me.brokenearthdev.simpleyaml.serialization;

import me.brokenearthdev.simpleyaml.exceptions.SerializationException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Base64;

/**
 * This class is responsible for deserializing a {@link String} to
 * an {@link Object}
 *
 * @author BrokenEarth // BrokenEarthDev
 * @version 1.0
 * @since 1.0
 */
public class Deserializer {

    /**
     * Deserializes the specified {@link String} into an
     * {@link Object}
     *
     * @param string The serialized string
     * @return The deserialized object
     * @throws SerializationException Thrown if there is a trouble deserializing
     */
    public Object deserialize(String string) throws SerializationException {
        try {
            byte[] bytes = Base64.getDecoder().decode(string);
            ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
            ObjectInputStream in = new ObjectInputStream(inputStream);
            Object deserialized = in.readObject();
            in.close();
            return deserialized;
        } catch (Exception e) {
            throw new SerializationException(e);
        }
    }

}
