/*
 * Copyright 2018 github.com/BrokenEarthDev
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */
package me.brokenearthdev.simpleyaml.serialization;

import me.brokenearthdev.simpleyaml.exceptions.SerializationException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Base64;

/**
 * This class is responsible for serializing objects
 *
 * @author BrokenEarth // BrokenEarthDev
 * @version 1.0
 * @since 1.0
 */
public class Serializer {

    /**
     * Serializes the specified object if they are serializable. If
     * the object is not serializable, {@link SerializationException}
     * will be thrown
     *
     * @param o The object to serialize
     * @return The serialized object as a byte array
     * {@link ObjectOutputStream}
     */
    public byte[] serialize(Object o)  {
        if (!Serialization.isSerializable(o))
            throw new SerializationException("Class is not serializable");
        try {
            ByteArrayOutputStream outstream = new ByteArrayOutputStream();
            ObjectOutputStream stream = new ObjectOutputStream(outstream);
            stream.writeObject(o);
            stream.close();
            return outstream.toByteArray();
        } catch (Exception e) {
            throw new SerializationException(e);
        }
    }



}
