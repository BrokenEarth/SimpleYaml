/*
 * * Copyright 2017-2018 github.com/BrokenEarthDev
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.brokenearthdev.simpleyaml.events;

import me.brokenearthdev.simpleyaml.api.Yaml;
import me.brokenearthdev.simpleyaml.api.YamlCloner;

import java.io.File;
import java.io.InputStream;

/**
 * This event is called when a yaml is cloned by the {@link YamlCloner}
 *
 * @author BrokenEarth // BrokenEarthDev
 * @version 1.0
 * @since 1.0
 */
public class YamlCloneEvent extends Event {

    /**
     * The output file specified by the constructor
     */
    private File result;
    /**
     * The {@link InputStream} specified by the constructor
     */
    private InputStream stream;
    /**
     * The cloned {@link Yaml} specified by the constructor
     */
    private Yaml cloned;

    /**
     * @param result The output file which is where the Yaml is cloned to
     * @param input The {@link InputStream} that is copied to the output file
     * @param cloned The cloned yaml
     */
    public YamlCloneEvent(File result, InputStream input, Yaml cloned) {
        this.result = result;
        this.stream = input;
        this.cloned = cloned;
    }

    /**
     * @return The output file which is where the Yaml is cloned to
     */
    public File getOutputFile() {
        return result;
    }

    /**
     * @return The {@link InputStream} that is copied to the output file {@link #getOutputFile()}
     */
    public InputStream getInputStream() {
        return stream;
    }

    /**
     * @return The cloned yaml
     */
    public Yaml getClonedYaml() {
        return cloned;
    }
}
