/*
 * * Copyright 2017-2018 github.com/BrokenEarthDev
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.brokenearthdev.simpleyaml.events;

import me.brokenearthdev.simpleyaml.api.Yaml;

import java.io.File;

/**
 * This exception is called when a {@link Yaml} is saved
 *
 * @author BrokenEarth // BrokenEarthDev
 * @version 1.0
 * @since 1.0
 */
public class YamlSaveEvent extends Event {

    /**
     * The affected {@link Yaml} specified by the constructor
     */
    private Yaml yaml;
    /**
     * The new data specified by the constructor
     */
    private String saved;
    /**
     * The saved {@link File} specified by the constructor
     */
    private File file;

    /**
     * @param yaml The affected {@link Yaml}
     * @param saved The new data
     * @param file The saved {@link File}
     */
    public YamlSaveEvent(Yaml yaml, String saved, File file) {
        this.yaml = yaml;
        this.saved = saved;
        this.file = file;
    }

    /**
     * @return The affected {@link Yaml}
     */
    public Yaml getYaml() {
        return yaml;
    }

    /**
     * @return The new data
     */
    public String getSavedData() {
        return saved;
    }

    /**
     * @return The saved file
     */
    public File getFile() {
        return file;
    }

    /**
     * @return If the yaml is saved to a file
     */
    public boolean fileSaved() {
        return file != null;
    }

}
