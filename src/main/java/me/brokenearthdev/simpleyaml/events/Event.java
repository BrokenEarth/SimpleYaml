/*
 * * Copyright 2017-2018 github.com/BrokenEarthDev
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.brokenearthdev.simpleyaml.events;

import me.brokenearthdev.eventbus.entities.EventBus;
import me.brokenearthdev.simpleyaml.api.Yaml;

/**
 * This class is the superclass (above) of all events. This event is controlled
 * by the {@link EventBus} in {@link Yaml#EVENT_BUS}.
 * Any event method with {@link Event} as their parameter will be called when an
 * event is called by {@link EventBus#callEvent(Object)}
 *
 * @author BrokenEarth // BrokenEarthDev
 * @version 1.0
 * @since 1.0
 */
public class Event {
}
