/*
 * * Copyright 2017-2018 github.com/BrokenEarthDev
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.brokenearthdev.simpleyaml.parser;

import me.brokenearthdev.simpleyaml.api.YamlOptions;
import me.brokenearthdev.simpleyaml.exceptions.YamlException;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.error.YAMLException;

/**
 * This class is responsible for parsing options. This class
 * converts {@link YamlOptions} to {@link DumperOptions}
 *
 * @author BrokenEarth // BrokenEarthDev
 * @version 1.0
 * @since 1.0
 */
public class YamlOptionsParser {

    /**
     * The {@link YamlOptions} specified in the constructor
     */
    private YamlOptions options;

    /**
     * The {@link DumperOptions} initialized by the constructor
     */
    private DumperOptions dumperOptions;

    /**
     * @param options The {@link YamlOptions} instance
     */
    public YamlOptionsParser(YamlOptions options) {
        this.options = options;
        this.dumperOptions = new DumperOptions();
    }

    /**
     * Parses all options from {@link YamlOptions} and returns
     * the {@link DumperOptions}
     *
     * @return The {@link DumperOptions}
     */
    public DumperOptions parseAll() {
        this.parseIndent();
        this.parseLineBreak();
        this.parseTimeZone();
        this.parseScalarStyle();
        this.parseFlowStyle();
        this.parseAllowUnicode();
        return dumperOptions;
    }

    /**
     * Parses the indents
     */
    public void parseIndent() {
        try {
            dumperOptions.setIndent(options.getIndent());
        } catch (YAMLException e) {
            throw new YamlException(e.getMessage());
        }
    }

    /**
     * Parses the {@link me.brokenearthdev.simpleyaml.api.YamlOptions.ScalarStyle}
     */
    public void parseLineBreak() {
        if (options.getLineBreak() == YamlOptions.LineBreak.MAC) dumperOptions.setLineBreak(DumperOptions.LineBreak.MAC);
        else if (options.getLineBreak() == YamlOptions.LineBreak.WIN) dumperOptions.setLineBreak(DumperOptions.LineBreak.WIN);
        else dumperOptions.setLineBreak(DumperOptions.LineBreak.UNIX);
    }

    /**
     * Parses the {@link java.util.TimeZone}
     */
    public void parseTimeZone() {
        dumperOptions.setTimeZone(options.getTimeZone());
    }

    /**
     * Parses the {@link me.brokenearthdev.simpleyaml.api.YamlOptions.ScalarStyle}
     */
    public void parseScalarStyle() {
       if (options.getScalarStyle().getChar() != 0) dumperOptions.setDefaultScalarStyle(DumperOptions.ScalarStyle.createStyle(options.getScalarStyle().getChar()));
       else dumperOptions.setDefaultScalarStyle(DumperOptions.ScalarStyle.PLAIN);
    }

    /**
     * Parses the {@link me.brokenearthdev.simpleyaml.api.YamlOptions.FlowStyle}
     */
    public void parseFlowStyle() {
        if (options.getFlowStyle() == YamlOptions.FlowStyle.AUTO) dumperOptions.setDefaultFlowStyle(DumperOptions.FlowStyle.AUTO);
        else if (options.getFlowStyle() == YamlOptions.FlowStyle.BLOCK) dumperOptions.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        else dumperOptions.setDefaultFlowStyle(DumperOptions.FlowStyle.FLOW);
    }

    /**
     * Parses the "Allow unicode" option
     */
    public void parseAllowUnicode() {
        dumperOptions.setAllowUnicode(options.isAllowUnicode());
    }



}
