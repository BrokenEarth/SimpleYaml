/*
 * * Copyright 2017-2018 github.com/BrokenEarthDev
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.brokenearthdev.simpleyaml.parser;

import me.brokenearthdev.simpleyaml.entities.YamlParser;
import me.brokenearthdev.simpleyaml.exceptions.YamlSyntaxException;
import me.brokenearthdev.simpleyaml.utils.CachedData;
import me.brokenearthdev.simpleyaml.utils.CachedFileData;
import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class is an implementation of {@link YamlParser}
 *
 * @author BrokenEarth // BrokenEarthDev
 * @version 1.0
 * @since 1.0
 */
public class YamlParserImpl implements YamlParser {

    private CachedData data;
    private Yaml yaml;
    private String string;

    private Map<?, ?> parentKeys;

    public YamlParserImpl(InputStream stream) {
        this.data = new CachedFileData(stream);
        this.yaml = new Yaml();
        this.string = data.getContents();
        parentKeys = mapParentKeys();
    }

    public YamlParserImpl(String string) {
        this.string = string;
        this.yaml = new Yaml();
        parentKeys = mapParentKeys();
    }

    private Map<?, ?> mapParentKeys() {
        try {
            return yaml.load(string);
        } catch (Exception e) {
            try {
                throw new YamlSyntaxException("Invalid syntax");
            } catch (YamlSyntaxException e1) {
                e1.printStackTrace();
                e.printStackTrace();
                return null;
            }
        }
    }



    @Override
    public Map<?, ?> getParentKeys() {
        return parentKeys;
    }

    @Override
    public void reloadKeys(String str) {
        this.string = str;
        parentKeys = mapParentKeys();
    }

    @Override
    public String getString() {
        return string;
    }

    private Object get(List<String> path, Map newMap, int index, boolean first, String expected) {
        Map map = (first) ? parentKeys : newMap;
        if (map == null) return null;
        String lastIndex = path.get(path.size() - 1);
        if (expected.equals(lastIndex)) {
            return map.get(expected);
        }
        for (Object object : map.keySet()) {
            if (!object.equals(expected)) continue;
            if (map.get(object) instanceof Map) {
                Map objMap = (Map) map.get(object);
                return get(path, objMap, index + 1, false, path.get(index + 1));
            }
        }
        return null;
    }

    @Override
    public Object get(List<String> path) {
        return get(path, new HashMap(), 0, true, path.get(0));
    }

    @Override
    public Object get(String path, char separator) {
        if (path.endsWith(separator + "") || path.startsWith(separator + "")) return null;
        char[] chars = path.toCharArray();
        List<String> paths = new ArrayList<>();
        String total = "";
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] != separator) {
                total += chars[i];
            }
            if (chars[i] == separator || i + 1 == chars.length) {
                paths.add(total);
                total = "";
            }
        }
        return get(paths);
    }

    @Override
    public List<String> getNestedPaths(String path, final char separator) {
        StringBuilder pathBuilder = new StringBuilder();
        List<String> paths = new ArrayList<>();
        char[] chars = path.toCharArray();
        if (path.startsWith(separator + "") || path.endsWith(separator + ""))
            throw new YamlSyntaxException("Path can't start or end with a separator");
        if (chars.length == 0 || chars[chars.length - 1] == separator || chars[0] == separator) return null;
        int recentSeparator = 0;
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] != separator) {
                recentSeparator = 0;
                pathBuilder.append(chars[i]);
            }
            if (chars[i] == separator || i + 1 == chars.length) {
                recentSeparator++;
                if (recentSeparator >= 2)
                    throw new YamlSyntaxException("There can't be a path separator repeated 2 or more times in a row");
                String total = pathBuilder.toString();
                pathBuilder.setLength(0);
                paths.add(total);
            }
        }
        return paths;
    }

    @Override
    public Map<String, Object> drawNested(List<String> paths, int index, Object expected) {
        Map<String, Object> map = new HashMap<>();
        String current = paths.get(index);
        if (index == paths.size() - 1) {
            map.put(current, expected);
            return map;
        }
        Map<String, Object> newMap = drawNested(paths, index + 1, expected);
        map.put(current, newMap);
        return map;
    }

}
