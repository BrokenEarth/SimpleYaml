/*
 * * Copyright 2017-2018 github.com/BrokenEarthDev
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.brokenearthdev.simpleyaml.entities;

import java.util.List;
import java.util.Map;

/**
 * A YAML reader which parses content into Java's primitive/wrapper classes,
 * aside from Lists and other helping methods
 *
 * @author ReflxctionDev | BrokenEarthDev
 * @version 1.0
 * @since 1.0
 */
public interface YamlReader {

    /**
     * Retrieves an object from the given path
     *
     * @param path Path to retrieve from
     * @return The object from the path (long, int, String, etc)
     */
    Object get(String path);

    /**
     * Retrieves a deserialized object
     *
     * @param path The path to retrieve from
     * @param as The expected class type ro retrieve
     *           from.
     * @param <T> Depends on the parameter that requires
     *           a {@link Class}.
     * @return The deserialized object
     */
    <T> T get(String path, Class<T> as);

    /**
     * Retrieves an {@link int} from the given path
     *
     * @param path Path to retrieve from
     * @return The integer from the given path
     */
    int getInt(String path);

    /**
     * Retrieves a {@code double} from the given path
     *
     * @param path Path to retrieve from
     * @return The double from the given path
     */
    double getDouble(String path);

    /**
     * Retrieves a {@code float} from the given path
     *
     * @param path Path to retrieve from
     * @return The float from the given path
     */
    float getFloat(String path);

    /**
     * Retrieves a {@code short} from the given path
     *
     * @param path Path to retrieve from
     * @return The short from the given path
     */
    short getShort(String path);

    /**
     * Retrieves a {@code long} from the given path
     *
     * @param path Path to retrieve from
     * @return The long from the given path
     */
    long getLong(String path);

    /**
     * Retrieves a {@link String} from the given path
     *
     * @param path Path to retrieve from
     * @return The String from the given path
     */
    String getString(String path);

    /**
     * Retrieves a {@code boolean} from the given path
     *
     * @param path Path to retrieve from
     * @return The boolean from the given path
     */
    boolean getBoolean(String path);

    /**
     * Whether the value from the given path is an {@code int}
     *
     * @param path Path to check from
     * @return {@code true} if the path's value is an {@code int}, or {@code false} if
     * otherwise
     */
    boolean isInteger(String path);

    /**
     * Whether the value from the given path is a {@code double}
     *
     * @param path Path to check from
     * @return {@code true} if the path's value is a {@code double}, or {@code false} if
     * otherwise
     */
    boolean isDouble(String path);

    /**
     * Whether the value from the given path is a {@code float}
     *
     * @param path Path to check from
     * @return {@code true} if the path's value is a {@code float}, or {@code false} if
     * otherwise
     */
    boolean isFloat(String path);

    /**
     * Whether the value from the given path is a {@code short}
     *
     * @param path Path to check from
     * @return {@code true} if the path's value is a {@code short}, or {@code false} if
     * otherwise
     */
    boolean isShort(String path);

    /**
     * Whether the value from the given path is a {@code long}
     *
     * @param path Path to check from
     * @return {@code true} if the path's value is a {@code long}, or {@code false} if
     * otherwise
     */
    boolean isLong(String path);

    /**
     * Whether the value from the given path is a {@code boolean}
     *
     * @param path Path to check from
     * @return {@code true} if the path's value is a {@code boolean}, or
     * {@code false} if otherwise
     */
    boolean isBoolean(String path);

    /**
     * Retrieves a {@link List} from the given path
     *
     * @param path Path to retrieve from
     * @return The list containing the appropriate object
     */
    List<?> getList(String path);

    /**
     * Retrieves a {@link List} containing {@link String} from the given
     * path
     *
     * @param path Path to retrieve from
     * @return The list with the {@link String} elements
     */
    List<String> getStringList(String path);

    /**
     * Retrieves a {@link List} containing {@link Integer} from the given
     * path
     *
     * @param path Path to retrieve from
     * @return The list with the {@link Integer} elements
     */
    List<Integer> getIntegerList(String path);

    /**
     * Retrieves a {@link List} containing {@link Double} from the given
     * path
     *
     * @param path Path to retrieve from
     * @return The list with the {@link Double} elements
     */
    List<Double> getDoubleList(String path);

    /**
     * Retrieves a {@link List} containing {@link Float} from the given
     * path
     *
     * @param path Path to retrieve from
     * @return The list with the {@link Float} elements
     */
    List<Float> getFloatList(String path);

    /**
     * Retrieves a {@link List} containing {@link Short} from the given
     * path
     *
     * @param path Path to retrieve from
     * @return The list with the {@link Short} elements
     */
    List<Short> getShortList(String path);

    /**
     * Retrieves a {@link List} containing {@link Long} from the given
     * path
     *
     * @param path Path to retrieve from
     * @return The list with the {@link Long} elements
     */
    List<Long> getLongList(String path);

    /**
     * Whether the value from the given path is an {@link List}
     *
     * @param path Path to check from
     * @return {@code true} if the path's value is a valid {@link List}, or {@code false} if
     * otherwise
     */
    boolean isList(String path);

    /**
     * Retrieves a {@link Map} from the given path
     * @param path Path to retrieve from
     * @return The Map from the given path
     */
    Map<?, ?> getMap(String path);

    /**
     * Whether the value from the given path is a {@link Map}
     *
     * @param path Path to check from
     * @return {@code true} if the path's value is a valid {@link Map}, or {@code false} if
     * otherwise
     */
    boolean isMap(String path);

    /**
     * @param path The object's path
     * @param as The type
     * @param deserialize Whether if the method will deserialize the
     *                    object and then check if its type is equal
     *                    to the required type
     * @param <T> The type
     * @return Whether if the specified type is equal to the
     * type specified
     */
    default <T> boolean is(String path, Class<T> as, boolean deserialize) {
        if (deserialize) {
            T got = get(path, as);
            return as.isInstance(got);
        }
        return as.isInstance(get(path));
    }

    /**
     * @param path The object's path
     * @param class_ The type
     * @param <T> The type
     * @return Whether if the specified object's path is equal to the
     * type specified
     */
    default <T> boolean is(String path, Class<T> class_) {
        return is(path, class_, false);
    }

    /**
     * Gets the value from the given path, or if the value doesn't exist, it creates the path
     * and saves the value
     *
     * @param path Path to retrieve from, or set if the value doesn't exist.
     * @param defaultValue The value to set if the path is invalid
     * @return The object from the path, or the default value specified if the path doesn't exist.
     */
    Object getOrSetIfNotExists(String path, Object defaultValue);

}
