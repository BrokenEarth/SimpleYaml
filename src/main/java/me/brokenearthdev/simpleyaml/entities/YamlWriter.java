/*
 * * Copyright 2017-2018 github.com/BrokenEarthDev
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.brokenearthdev.simpleyaml.entities;

import java.io.File;
import java.io.IOException;

/**
 * Represents a YAML writer, for adding content to a YML file
 *
 * @author ReflxctionDev | BrokenEarthDev
 * @version 1.0
 * @since 1.0
 */
public interface YamlWriter {

    /**
     * Adds a comment to the file, on the last possible line
     *
     * @param text Content of the comment
     */
    void addComment(String text);

    /**
     * Sets the value of the object that is in the given path
     *
     * @param path  Path of the object
     * @param value New value to set
     */
    void set(String path, Object value);

    /**
     * Removes a path from the file
     *
     * @param path Path to remove
     */
    void remove(String path);

    /**
     * Saves the yaml file after being modified.
     *
     * @param file File to save
     * @throws IOException If there were issues while saving the file
     */
    void save(File file) throws IOException;

    /**
     * Saves the content of the YML file
     *
     * @param file     File to save for
     * @param override Whether it should override the old content
     * @throws IOException If there were issues while saving the file
     */
    void save(File file, boolean override) throws IOException;

    String saveToString(boolean override);

    String saveToString();

    /**
     * Gets the value from the given path, or if the value doesn't exist, it creates the path
     * and saves the value
     *
     * @param path Path to retrieve from, or set if the value doesn't exist.
     * @param defaultValue The value to set if the path is invalid
     * @return The object from the path, or the default value specified if the path doesn't exist.
     */
    Object getOrSetIfNotExists(String path, Object defaultValue);
}
