/*
 * * Copyright 2017-2018 github.com/BrokenEarthDev
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.brokenearthdev.simpleyaml.entities;

import java.util.List;
import java.util.Map;

/**
 * This class is responsible for parsing yaml and mapping it
 *
 * @author BrokenEarth // BrokenEarthDev
 * @version 1.0
 * @since 1.0
 */
public interface YamlParser {

    /**
     * @return The mapped parent keys. Their children will be in
     * the value slot as a {@link java.util.HashMap}
     */
    Map<?, ?> getParentKeys();

    /**
     * Reloads the parent keys. This is useful if any data is changed
     * and you want to retrieve the updated data
     *
     * @param string The new string contents
     */
    void reloadKeys(String string);

    /**
     * @return The contents. Can be changed by {@link #reloadKeys(String)}
     */
    String getString();

    /**
     * @param path The paths as a list. Each list entry is a sub-path
     * @return The found object. If the object is not found, the method
     * will return null
     */
    Object get(List<String> path);

    /**
     * @param path The paths separated by the specified separator
     * @param separator The separator. A placed separator means that
     *                  the next set of characters before their separator is
     *                  the child of the last set of characters after their
     *                  separator
     *                  <pre>
     *                      get("me.brokenearthdev", '.');
     *                      is like:
     *                      me:
     *                        brokenearthdev: (value)
     *                  </pre>
     * @return The found object. If the object is not found, the method will
     * return null
     */
    Object get(String path, char separator);

    /**
     * Converts the paths separated by a separator to a {@link List}
     *
     * @param path The paths separated by the specified separator
     * @param separator The separator. A placed separator means that
     *                  the next set of characters before their separator is
     *                  the child of the last set of characters after their
     *                  separator
     *                  <pre>
     *                      get("me.brokenearthdev", '.');
     *                      is like:
     *                      me:
     *                        brokenearthdev: (value)
     *                  </pre>
     * @return The converted paths that is separated by a separator to a {@link List}
     */
    List<String> getNestedPaths(String path, final char separator);

    /**
     * Converts the {@link List} path to nested path (if there are subpaths) with
     * the specified {@link Object} as the value
     *
     * @param paths The paths. The higher the index, the more nested paths there is.
     *              The super-path will be in index 0, subpath will be in index 1, etc.
     * @param index The index where the method will start to draw nested. At index 0,
     *              the method will draw nested for paths.
     * @param expected The value
     * @return The converted {@link List} path to nested path (id there are subpaths) with
     * the specified {@link Object} as the value
     */
    Map<String, Object> drawNested(List<String> paths, int index, Object expected);
}
