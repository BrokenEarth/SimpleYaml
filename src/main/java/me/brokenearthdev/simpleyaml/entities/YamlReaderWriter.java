/*
 * Copyright 2018 github.com/BrokenEarthDev
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */

package me.brokenearthdev.simpleyaml.entities;

import me.brokenearthdev.simpleyaml.api.Yaml;
import me.brokenearthdev.simpleyaml.events.YamlLoadEvent;
import me.brokenearthdev.simpleyaml.events.YamlSaveEvent;
import me.brokenearthdev.simpleyaml.exceptions.YamlException;
import me.brokenearthdev.simpleyaml.exceptions.YamlSyntaxException;
import me.brokenearthdev.simpleyaml.parser.YamlOptionsParser;
import me.brokenearthdev.simpleyaml.parser.YamlParserImpl;
import me.brokenearthdev.simpleyaml.serialization.Serialization;
import me.brokenearthdev.simpleyaml.utils.CachedFileData;
import me.brokenearthdev.simpleyaml.utils.CachedStringData;
import me.brokenearthdev.simpleyaml.utils.FileWriter;
import org.yaml.snakeyaml.DumperOptions;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.*;

/**
 * An implementation of the {@link YamlWriter} and {@link YamlReader} interface
 *
 * @author BrokenEarth // BrokenEarthDev
 * @version 1.0
 * @since 1.0
 */
public class YamlReaderWriter implements YamlReader, YamlWriter {

    private YamlParser parser;
    private Map<String, Object> dataMap = new HashMap<>();
    private String toWriteAdditional = "";
    private Map<?, ?> fileData;
    private Yaml config;
    private YamlOptionsParser optionsParser;
    protected File file;
    private URL url;

    public YamlReaderWriter(YamlParser parser) {
        if (!(this instanceof Yaml))
            throw new YamlException("Cannot construct a reader/writer when this isn't an instance of " + Yaml.class.getName());
        this.parser = parser;
        Map<?, ?> keys = parser.getParentKeys();
        this.fileData = (keys != null) ? keys : new HashMap<>();
        this.config = (Yaml) this;
        Yaml.EVENT_BUS.callEvent(new YamlLoadEvent(config, parser));
    }

    public YamlReaderWriter(YamlParser parser, File file) {
        if (!(this instanceof Yaml))
            throw new YamlException("Cannot construct a reader/writer when this isn't an instance of " + Yaml.class.getName());
        if (!file.exists())
            createNewFile(file);
        this.parser = parser;
        Map<?, ?> keys = parser.getParentKeys();
        this.fileData = (keys != null) ? keys : new HashMap<>();
        this.config = (Yaml) this;
        this.file = file;
        Yaml.EVENT_BUS.callEvent(new YamlLoadEvent(config, parser));
    }

    public YamlReaderWriter(YamlParser parser, URL url) {
        if (!(this instanceof Yaml))
            throw new YamlException("Cannot construct a reader/writer when this isn't an instance of " + Yaml.class.getName());
        this.parser = parser;
        Map<?, ?> keys = parser.getParentKeys();
        this.fileData = (keys != null) ? keys : new HashMap<>();
        this.config = (Yaml) this;
        this.url = url;
        Yaml.EVENT_BUS.callEvent(new YamlLoadEvent(config, parser));
    }

    protected void reloadOptions() {
        if (optionsParser == null) optionsParser = new YamlOptionsParser(config.getOptions());
        DumperOptions options = optionsParser.parseAll();
        this.yaml = new org.yaml.snakeyaml.Yaml(options);
    }

    @Override
    public Object get(String path) {
        if (file != null)
            doReloadFile();
        if (url != null)
            doReloadURL();
        return parser.get(path, config.getOptions().getSeparator());
    }

    @Override
    public <T> T get(String path, Class<T> as) {
        return (T) Serialization.DESERIALIZER.deserialize(get(path).toString());
    }

    @Override
    public int getInt(String path) {
        Object value = get(path);
        return (value instanceof Integer) ? (Integer) value : 0;
    }

    @Override
    public double getDouble(String path) {
        Object value = get(path);
        return (value instanceof Double) ? (Double) value : 0;
    }

    @Override
    public float getFloat(String path) {
        Object value = get(path);
        return (value instanceof Float) ? (Float) value : 0;
    }

    @Override
    public short getShort(String path) {
        Object value = get(path);
        return (value instanceof Short) ? (Short) value : 0;
    }

    @Override
    public long getLong(String path) {
        Object value = get(path);
        return (value instanceof Long) ? (Long) value : 0;
    }

    @Override
    public String getString(String path) {
        Object value = get(path);
        return (value instanceof String) ? (String) value : null;
    }

    @Override
    public boolean getBoolean(String path) {
        Object value = get(path);
        return (value instanceof Boolean) && (boolean) value;
    }

    @Override
    public boolean isInteger(String path) {
        return get(path) instanceof Integer;
    }

    @Override
    public boolean isDouble(String path) {
        return get(path) instanceof Double;
    }

    @Override
    public boolean isFloat(String path) {
        return get(path) instanceof Float;
    }

    @Override
    public boolean isShort(String path) {
        return get(path) instanceof Short;
    }

    @Override
    public boolean isLong(String path) {
        return get(path) instanceof Long;
    }

    @Override
    public boolean isBoolean(String path) {
        return get(path) instanceof Boolean;
    }

    @Override
    public List<String> getStringList(String path) {
        List<?> value = getList(path);
        List<String> list = new ArrayList<>();
        value.stream().filter(v -> v instanceof String).forEach(v -> list.add((String) v));
        return list;
    }

    @Override
    public List<?> getList(String path) {
        Object value = get(path);
        return (value instanceof List) ? (List) value : new ArrayList<>();
    }

    @Override
    public List<Integer> getIntegerList(String path) {
        List<?> value = getList(path);
        List<Integer> list = new ArrayList<>();
        value.stream().filter(v -> v instanceof Integer).forEach(v -> list.add((Integer) v));
        return list;
    }

    @Override
    public List<Double> getDoubleList(String path) {
        List<?> value = getList(path);
        List<Double> list = new ArrayList<>();
        value.stream().filter(v -> v instanceof Double).forEach(v -> list.add((Double) v));
        return list;
    }

    @Override
    public List<Float> getFloatList(String path) {
        List<?> value = getList(path);
        List<Float> list = new ArrayList<>();
        value.stream().filter(v -> v instanceof Float).forEach(v -> list.add((Float) v));
        return list;
    }

    @Override
    public List<Short> getShortList(String path) {
        List<?> value = getList(path);
        List<Short> list = new ArrayList<>();
        value.stream().filter(v -> v instanceof Short).forEach(v -> list.add((Short) v));
        return list;
    }

    @Override
    public List<Long> getLongList(String path) {
        List<?> value = getList(path);
        List<Long> list = new ArrayList<>();
        value.stream().filter(v -> v instanceof Long).forEach(v -> list.add((Long) v));
        return list;
    }

    @Override
    public boolean isList(String path) {
        return get(path) instanceof List;
    }

    @Override
    public Map<?, ?> getMap(String path) {
        Object value = get(path);
        return (value instanceof Map) ? (Map<?, ?>) value : new HashMap<>();
    }

    @Override
    public boolean isMap(String path) {
        return get(path) instanceof Map;
    }

    @Override
    public void addComment(String text) {
        CachedStringData data = new CachedStringData(text);
        List<String> lines = data.getLines();
        StringBuilder total = new StringBuilder();
        if (!toWriteAdditional.equals("")) total.append("\n");
        for (int i = 0; i < lines.size(); i++) {
            total.append("#").append(lines.get(i));
            if (i != lines.size() - 1) total.append("\n");
        }
        this.toWriteAdditional = total.toString();
    }

    @Override
    public void set(String path, Object value) {
        ensureLocked();
        reloadOptions();
        Objects.requireNonNull(path, "Path can't be null");
        if (path.replace(" ", "").equals("")) throw new YamlSyntaxException("Path can't be empty");
        if (value == null) {
            remove(path);
            return;
        }
        if (Serialization.isSerializable(value))
            value = serialize(value);
        List<String> paths = parser.getNestedPaths(path, config.getOptions().getSeparator());
        if (paths.size() != 1) dataMap.put(paths.get(0), parser.drawNested(paths, 1, value));
        else dataMap.put(path, value);
    }

    @Override
    public void remove(String path) {
        ensureLocked();
        reloadOptions();
        dataMap.remove(path);
        fileData.remove(path);
    }

    @Override
    public void save(File file) throws IOException {
        save(file, false);
    }

    @Override
    public void save(File file, boolean override) throws IOException {
        FileWriter writer = new FileWriter(file);
        String toWrite = saveString(override, true);
        writer.write(toWrite, true);
        Yaml.EVENT_BUS.callEvent(new YamlSaveEvent(config, toWrite, file));
    }

    @Override
    public String saveToString(boolean override) {
        return saveString(override, false);
    }

    @Override
    public String saveToString() {
        return saveToString(false);
    }

    private String serialize(Object o) {
        byte[] bytes = Serialization.SERIALIZER.serialize(o);
        return Base64.getEncoder().encodeToString(bytes);
    }

    private String saveString(boolean override, boolean fileCalled) {
        ensureLocked();
        reloadOptions();
        String header = getHeaderAsString();
        String footer = getFooterAsString();
        if (override) {
            String toDump = yaml.dump(dataMap);
            if (config.getOptions().getHeader() != null) {
                if (toDump == null || toDump.equals("")) toDump = "" + header + "\n";
                else toDump = header + "\n" + toDump;
            }
            if (config.getOptions().getFooter() != null) {
                if (toDump == null || toDump.equals("")) toDump = "" + footer;
                else toDump = toDump + footer;
            }
            dataMap.clear();

            if (!fileCalled) Yaml.EVENT_BUS.callEvent(new YamlSaveEvent(config, toDump, null));
            return toDump;
        } else {
            Map<String, Object> toWrite = new LinkedHashMap<>();
            for (Object o : fileData.keySet()) {
                toWrite.put(o.toString(), fileData.get(o));
            }
            for (Object o : dataMap.keySet()) {
                toWrite.put(o.toString(), o);
            }
            String result = yaml.dump(toWrite);
            if (!toWriteAdditional.equals("")) {
                result += toWriteAdditional;
                toWriteAdditional = "";
            }
            if (config.getOptions().getHeader() != null) {
                if (result == null || result.equals("")) result = "" + header;
                else result = header + "\n" + result;
            }
            if (config.getOptions().getFooter() != null) {
                if (result == null || result.equals("")) result = "" + footer;
                else result = result + footer;
            }
            toWrite.clear();
            dataMap.clear();
            parser.reloadKeys(result);
            fileData = parser.getParentKeys();
            if (!fileCalled) Yaml.EVENT_BUS.callEvent(new YamlSaveEvent(config, result, null));
            return result;
        }
    }

    private org.yaml.snakeyaml.Yaml yaml;

    private String getHeaderAsString() {
        if (config.getOptions().getHeader() == null) return null;
        CachedStringData headerStringData = new CachedStringData(config.getOptions().getHeader());
        List<String> header = headerStringData.getLines();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < header.size(); i++) {
            if (i + 1 == header.size()) builder.append("# ").append(header.get(i));
            else builder.append("# ").append(header.get(i)).append("\n");
        }
        return builder.toString();
    }

    private String getFooterAsString() {
        if (config.getOptions().getFooter() == null) return null;
        CachedStringData footerStringData = new CachedStringData(config.getOptions().getFooter());
        List<String> footer = footerStringData.getLines();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < footer.size(); i++) {
            if (i + 1 == footer.size()) builder.append("# ").append(footer.get(i));
            else builder.append("# ").append(footer.get(i)).append("\n");
        }
        return builder.toString();
    }

    private void ensureLocked() {
        if (config.getOptions().isLocked())
            throw new YamlException("Yaml is locked");
    }

    private void reloadFile() throws IOException {
        CachedFileData data = new CachedFileData(new FileInputStream(file));
        this.parser = new YamlParserImpl(data.getContents());
    }

    private void reloadURL() throws IOException {
        CachedFileData data = new CachedFileData(url);
        this.parser = new YamlParserImpl(data.getStream());
    }

    private void doReloadFile() {
        try {
            reloadFile();
        } catch (IOException e) {
            throw new YamlException(e);
        }
    }

    private void doReloadURL() {
        try {
            reloadURL();
        } catch (IOException e) {
            throw new YamlException(e);
        }
    }


    public Object getOrSetIfNotExists(String path, Object defaultValue) {
        Object value = config.get(path);
        if (value == null) {
            config.set(path, defaultValue);
        }
        return value == null ? defaultValue : value;
    }

    private static File createNewFile(File file) {
        try {
            file.createNewFile();
            return file;
        } catch (IOException e) {
            throw new YamlException(e);
        }
    }

}