/*
 * * Copyright 2017-2018 github.com/BrokenEarthDev
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.brokenearthdev.simpleyaml.exceptions;

/**
 * This exception is thrown when the {@link me.brokenearthdev.simpleyaml.api.Yaml}
 * configuration is invalid
 *
 * @author BrokenEarth // BrokenEarthDev
 */
public class InvalidYamlException extends YamlException {

    public InvalidYamlException() {
        super();
    }

    public InvalidYamlException(String message) {
        super(message);
    }

    public InvalidYamlException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidYamlException(Throwable cause) {
        super(cause);
    }

    protected InvalidYamlException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
