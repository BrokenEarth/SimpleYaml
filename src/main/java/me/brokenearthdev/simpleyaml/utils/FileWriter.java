/*
 * * Copyright 2017-2018 github.com/BrokenEarthDev
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.brokenearthdev.simpleyaml.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public final class FileWriter {

    private File file;

    private CachedFileData data;

    public FileWriter(File file) throws IOException {
        this.file = file;
        this.data = new CachedFileData(file);
    }

    public void write(String text, boolean override) throws IOException {
        java.io.FileWriter fwriter = new java.io.FileWriter(file);
        BufferedWriter writer = new BufferedWriter(fwriter);
        String contents;
        if (override) contents = text;
        else contents = data.getContents() + text;
        writer.write(contents);
        writer.close();
        fwriter.close();
        data = new CachedFileData(file);
    }

    public void write(String text) throws IOException {
        write(text, false);
    }

    public void write(List<String> lines, boolean override) throws IOException {
        String contents = CachedFileData.getContentsFromList(lines);
        write(contents, override);
    }

    public void write(List<String> lines) throws IOException {
        write(lines, false);
    }

    public void writeLine(String line, boolean override) throws IOException {
        if (data.getContents().equals("")) write(line, override);
        else write("\n" + line, override);
    }

    public void writeLine(String line) throws IOException {
        writeLine(line, false);
    }

    public void replaceLineByIndex(int index, String newContent) throws IOException {
        List<String> list = data.getLines();
        List<String> newList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (i == index)
                newList.add(newContent);
            else newList.add(list.get(i));
        }
        write(newList);
    }

    public void replaceLineByLineNumber(int number, String newContent) throws IOException {
        replaceLineByIndex(number - 1, newContent);
    }

    public void replace(String oldContent, String newContent) throws IOException {
        List<String> list = data.getLines();
        List<String> newList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            newList.add(list.get(i).replace(oldContent, newContent));
        }
        write(newList);
    }

    public void removeLineByIndex(int index) throws IOException {
        List<String> str = data.getLines();
        List<String> newList = new ArrayList<>();
        for (int i = 0; i < str.size(); i++) {
            if (index == i) continue;
            newList.add(str.get(i));
        }
        write(newList);
    }

    public void removeLineByLineNumber(int lineNumber) throws IOException {
        removeLineByIndex(lineNumber - 1);
    }

}
