/*
 * * Copyright 2017-2018 github.com/BrokenEarthDev
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.brokenearthdev.simpleyaml.utils;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * This class is an implementation of {@link CachedData}.
 * It can store the data of an {@link InputStream}, {@link File},
 * and {@link URL}.
 * However, since {@link File} and {@link URL} are converted to {@link InputStream},
 * it can't be reloaded
 *
 * @author BrokenEarth // BrokenEarthDev
 * @version 1.0
 * @since 1.0
 */
public final class CachedFileData implements CachedData {

    private InputStream stream;

    private List<String> lines;
    private String contents;

    private File file = null;
    private URL url;

    private InputStreamReader streamReader;
    private BufferedReader reader;

    public CachedFileData(InputStream stream) {
        Objects.requireNonNull(stream, "Input stream can't be null");
        this.stream = stream;
        this.streamReader = new InputStreamReader(stream);
        this.reader = new BufferedReader(streamReader);
        reloadCache();
    }

    public CachedFileData(File file) throws IOException {
        this(new FileInputStream(file));
        this.file = file;
    }

    public CachedFileData(URL url) throws IOException {
        this(url.openStream());
        this.url = url;
    }

    public File getFile() {
        return file;
    }

    public boolean isFile() {
        return file != null;
    }

    public URL getURL() {
        return url;
    }

    public boolean isUrl() {
        return !isFile();
    }

    public InputStream getStream() {
        return stream;
    }

    private void reloadCache() {
        try {
            this.lines = loadLines();
            this.contents = loadContents();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<String> loadLines() throws IOException {
        List<String> lines = new ArrayList<>();

        String line;
        while ((line = reader.readLine()) != null) {
            lines.add(line);
        }
        return lines;
    }

    private String loadContents() {
        return getContentsFromList(lines);
    }

    @Override
    public List<String> getLines() {
        return lines;
    }



    @Override
    public String getContents() {
        return contents;
    }

    public String getLineByIndex(int index) {
        return lines.get(index);
    }

    public String getLineByNumber(int line) {
        return getLineByIndex(line - 1);
    }

    public static String getContentsFromList(List<String> strings) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < strings.size(); i++) {
            String toWrite = strings.get(i);
            if (i + 1 != strings.size()) toWrite += "\n";
            builder.append(toWrite);
        }
        return builder.toString();
    }

}