/*
 * * Copyright 2017-2018 github.com/BrokenEarthDev
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.brokenearthdev.simpleyaml.utils;

import java.util.List;

/**
 * All implementations of {@link CachedData} are going to have
 * two methods, {@link #getContents()} and {@link #getLines()}.
 * A {@link CachedData} can't be reloaded
 *
 * @author BrokenEarth // BrokenEarthDev
 */
public interface CachedData {

    /**
     * @return The raw contents of the specified data
     */
    String getContents();

    /**
     * @return A {@link List} of lines containing the data specified
     */
    List<String> getLines();

}
