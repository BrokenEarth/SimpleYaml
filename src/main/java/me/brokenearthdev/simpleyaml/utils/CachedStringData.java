/*
 * * Copyright 2017-2018 github.com/BrokenEarthDev
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.brokenearthdev.simpleyaml.utils;

import me.brokenearthdev.simpleyaml.utils.CachedData;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public final class CachedStringData implements CachedData {

    private String string;

    private String contents;
    private List<String> lines;

    public CachedStringData(String string) {
        this.string = string;
        this.reloadCache();
    }

    public String getString() {
        return string;
    }

    @Override
    public String getContents() {
        return string;
    }

    @Override
    public List<String> getLines() {
        return lines;
    }


    private void reloadCache() {
        try {
            this.contents = string;
            this.lines = loadLines();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<String> loadLines() throws IOException {
        StringReader sreader = new StringReader(string);
        BufferedReader reader = new BufferedReader(sreader);
        String current;
        List<String> lines = new ArrayList<>();
        while ((current = reader.readLine()) != null) {
            lines.add(current);
        }
        return lines;
    }

}
